'use strict';
document.addEventListener('DOMContentLoaded', ev => {
	const input = document.querySelector('.input');
	const email = document.querySelector('.inputEmail');
	email.addEventListener('input', e => {
		e.preventDefault();
		if(email.validity.typeMismatch){
			email.setCustomValidity('');
			input.classList.add('input--error');
			console.log('Wrong email format!');
		}
		else{
				if(input.classList.contains('input--error')){
					input.classList.remove('input--error');
				}
				console.log('Valid email format');
		}
	});
});